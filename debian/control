Source: golang-github-inconshreveable-muxado
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <pkg-go-maintainers@lists.alioth.debian.org>
Uploaders: Tim Potter <tpot@hpe.com>
Build-Depends: debhelper-compat (= 12), dh-golang, golang-any
Standards-Version: 3.9.6
Homepage: https://github.com/inconshreveable/muxado
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-inconshreveable-muxado
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-inconshreveable-muxado.git
XS-Go-Import-Path: github.com/inconshreveable/muxado

Package: golang-github-inconshreveable-muxado-dev
Architecture: all
Depends: ${misc:Depends}, ${shlibs:Depends}
Multi-Arch: foreign
Description: Stream multiplexing for Go
 What is stream multiplexing?
 .
 Imagine you have a single stream (a bi-directional stream of bytes) like a TCP
 connection. Stream multiplexing is a method for enabling the transmission of
 multiple simultaneous streams over the one underlying transport stream.
 .
 What is muxado?
 .
 muxado is an implementation of a stream multiplexing library in Go that can be
 layered on top of a net.Conn to multiplex that stream. muxado's protocol is not
 currently documented explicitly, but it is very nearly an implementation of the
 HTTP2 framing layer with all of the HTTP-specific bits removed. It is heavily
 inspired by HTTP2, SPDY, and WebMUX.
 .
 How does it work?
 .
 Simplifying, muxado chunks data sent over each multiplexed stream and transmits
 each piece as a "frame" over the transport stream. It then sends these frames,
 often interleaving data for multiple streams, to the remote side. The remote
 endpoint then reassembles the frames into distinct streams of data which are
 presented to the application layer.
 .
 What good is it anyways?
 .
 A stream multiplexing library is a powerful tool for an application developer's
 toolbox which solves a number of problems:
 .
   - It allows developers to implement asynchronous/pipelined protocols with
     ease. Instead of matching requests with responses in your protocols, just
     open a new stream for each request and communicate over that.
   - muxado can do application-level keep-alives and dead-session detection so
     that you don't have to write heartbeat code ever again.
   - You never need to build connection pools for services running your
     protocol. You can open as many independent, concurrent streams as you need
     without incurring any round-trip latency costs.
   - muxado allows the server to initiate new streams to clients which is
     normally very difficult without NAT-busting trickery.
 .
 This package contains the source.
